package com.test.streaming

import scopt.OptionParser

case class Config(inputDir: String = "",
                  checkPointDir: String = "",
                  outputDir: String = "",
                  triggerInterval: Long = 5000)

object ConfigParser {

  val parser: OptionParser[Config] = new scopt.OptionParser[Config]("scopt") {
    head("scopt", "3.x")

    opt[String]("input")
      .required()
      .action((x, c) => c.copy(inputDir = x))
      .text("Input streaming directory")

    opt[String]("checkpoints")
      .required()
      .action((x, c) => c.copy(checkPointDir = x))
      .text("Directory to store checkpoints")

    opt[String]("output")
      .required()
      .action((x, c) => c.copy(outputDir = x))
      .text("Output data storage directory")

    opt[Long]("trigg")
      .optional()
      .action((x, c) => c.copy(triggerInterval = x))
      .text("Streaming trigger interval")

  }

  def parseConfig(args: Array[String]) = {
    ConfigParser.parser.parse(args, Config()) match {
      case Some(config) =>
        config
      case None =>
        parser.showUsage()
        throw new RuntimeException("Config parse failed")
    }

  }

}
