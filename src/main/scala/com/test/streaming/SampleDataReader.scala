package com.test.streaming

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object SampleDataReader {

  /**
    * Run with following line:
    *
    * ./bin/spark-submit \
    * --name "data-reader" \
    * --class com.test.streaming.SampleDataReader \
    * streaming-1.0-SNAPSHOT.jar \
    * --input "s3a://streaming/stream/input"
    * --checkpoints "s3a://streaming/stream/input/checkpoints" \
    * --output "s3a://streaming/stream/output" \
    *
    **/
  def main(args: Array[String]): Unit = {

    implicit val config: Config = ConfigParser.parseConfig(args)

    //Init spark session
    val sparkConf = new SparkConf()
    val spark = SparkSession.builder()
      .master("local")
      .config(sparkConf)
      .appName("stream_processing")
      .getOrCreate()

    spark.read.parquet(s"${config.outputDir}/revenue/").show(100,false)
    spark.read.parquet(s"${config.outputDir}/user/").show(100,false)

  }

}
