package com.test.streaming

import org.apache.spark.SparkConf
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.{DataFrame, Dataset, Row, SaveMode, SparkSession}
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object Core {

  val schema = StructType(Seq(
    StructField("user", StringType),
    StructField("timestamp", StringType),
    StructField("spend", StringType),
    StructField("evtname", StringType)))

  /**
    * Run with following line:
    *
    * ./bin/spark-submit \
    * --name "stream-processor" \
    * --class com.test.streaming.Core \
    * streaming-1.0-SNAPSHOT.jar \
    * --input "s3a://streaming/stream/input"
    * --checkpoints "s3a://streaming/stream/input/checkpoints" \
    * --output "s3a://streaming/stream/output" \
    *
    **/
  def main(args: Array[String]): Unit = {

    implicit val config: Config = ConfigParser.parseConfig(args)

    //Init spark session
    val sparkConf = new SparkConf()
    val spark = SparkSession.builder()
      .master("local")
      .config(sparkConf)
      .appName("streaming")
      .getOrCreate()

    //Setup stream reader
    spark.readStream
      .schema(schema)
      .json(config.inputDir)
      .writeStream
      .foreachBatch((batch: Dataset[Row], _) => {
        batch.persist() //persist batch to be processed twice
        writeRevenue(batch)
        writeUser(batch)
        batch.unpersist() //free up space
      }).option("checkpointLocation", config.checkPointDir) //Remember already processed files
      .trigger(Trigger.ProcessingTime(config.triggerInterval)) //Configure triggering policy
      .start()
      .awaitTermination()
  }

  def writeRevenue(df: DataFrame)(implicit config: Config) = {
    df.select("user", "spend")
      .write
      .mode(SaveMode.Append)
      .parquet(s"${config.outputDir}/revenue")
  }


  def writeUser(df: DataFrame)(implicit config: Config) = {
    df.select("user", "timestamp", "evtname")
      .write
      .mode(SaveMode.Append)
      .parquet(s"${config.outputDir}/user")
  }


}
