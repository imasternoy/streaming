# Task

Assume we already have real time streaming data coming to our AWS S3, write some code to process the real time streaming data and have it REAL-TIME available in tables? (You can use Spark, Scala, Java, Python, etc. whatever the coding language you want)
Assume the input data source folder is: “s3://data/test”. Inside the “test” folder, it will have thousands of json files like: 0000.json, 0001.json, 0002.json ...
To simplify, example records JSON will look as follows:

```
{“user”: “A”, “timestamp”:”2017-12-19 10:41:49”, “spend”:”1.99”,”evtname”:”iap”}
{“user”: “B”, “timestamp”:”2017-12-20 11:22:18”, ”evtname”:”tutorial”}
{“user”: “A”, “timestamp”:”2017-12-20 18:20:10”, “spend”:”9.99”,”evtname”:”iap”}
```

Output: two tables (all data types are string)
table user. column: user, timestamp, evtname
table revenue column: user, total_spend

# Installation

```sh
$ git clone https://imasternoy@bitbucket.org/imasternoy/streaming.git
$ mvn clean install
```

Setup Spark environment and Input params:
```
$ export AWS_ACCESS_KEY	<VALUE>
$ export AWS_ACCESS_KEY_ID	<VALUE>
$ export AWS_SECRET_ACCESS_KEY	<VALUE>
$ export AWS_SECRET_KEY	<VALUE>
```

```
$ cd spark-2.4.3/
$ ./bin/spark-submit \
		--name "data-reader" \
		--class com.test.streaming.Core \
		streaming-1.0-SNAPSHOT.jar \
		--input "s3a://streaming/stream/input"
		--checkpoints "s3a://streaming/stream/input/checkpoints" \
		--output "s3a://streaming/stream/output" \
```

Application was tested with Spark 2.4.3. Distribution is available here: 
```
$ curl https://archive.apache.org/dist/spark/spark-2.4.3/spark-2.4.3-bin-hadoop2.7.tgz > spark-2.3.0-bin-hadoop2.7.tgz
```

# Job run

Job creates a streaming pipeline which polls configured amazon s3 input bucket. Each time it detects new files it runs two writes into user and revenue tables.
Both tables are represented as an another folders with parquete files inside.
Application provides SampleDataReader class which can be used as a template to access data output.


